import "./style.css";
import * as THREE from "three";
import { OrbitControls } from "three/examples/jsm/controls/OrbitControls.js";
import * as dat from "dat.gui";
/**
 * Base
 */
// Debug
const gui = new dat.GUI();

// Canvas
const canvas = document.querySelector("canvas.webgl");

// Scene
const scene = new THREE.Scene();

// Fog
const fog = new THREE.Fog("#262837", 10, 20);
scene.fog = fog;
/**
 * Textures
 */
const textureLoader = new THREE.TextureLoader();
const doorColorTexture = textureLoader.load("/textures/door/color.jpg");
const doorAmbientOcclusionTexture = textureLoader.load(
  "/textures/door/ambientOcclusion.jpg"
);
const doorHeightTexture = textureLoader.load("/textures/door/height.jpg");
const doorMetalnessTexture = textureLoader.load("/textures/door/metalness.jpg");
const doorNormalTexture = textureLoader.load("/textures/door/normal.jpg");
const doorRoughnessTexture = textureLoader.load("/textures/door/roughness.jpg");
const doorAlphaTexture = textureLoader.load("/textures/door/alpha.jpg");

const bricksColorTexture = textureLoader.load("/textures/bricks/color.jpg");
const bricksAmbientOcculsionTexture = textureLoader.load(
  "/textures/bricks/ambientOcclusion.jpg"
);
const bricksNormalTexture = textureLoader.load("/textures/bricks/normal.jpg");
const bricksRoughnessTexture = textureLoader.load(
  "/textures/bricks/roughness.jpg"
);

const graveyardTexture = textureLoader.load(
  "/textures/bricks/graveyardText.jpg"
);

const roofColorTexture = textureLoader.load("/textures/bricks/color.jpg");
const roofAmbientOcculsionTexture = textureLoader.load(
  "/textures/bricks/ambientOcclusion.jpg"
);
const roofNormalTexture = textureLoader.load("/textures/bricks/normal.jpg");
const roofRoughnessTexture = textureLoader.load(
  "/textures/bricks/roughness.jpg"
);

roofColorTexture.rotation = -0.12;
roofColorTexture.center.set(100, 100);
roofColorTexture.wrapS = THREE.RepeatWrapping;
roofColorTexture.wrapT = THREE.RepeatWrapping;

roofNormalTexture.rotation = -0.12;
roofNormalTexture.center.set(100, 100);
roofNormalTexture.wrapS = THREE.RepeatWrapping;
roofNormalTexture.wrapT = THREE.RepeatWrapping;

roofAmbientOcculsionTexture.rotation = -0.12;
roofAmbientOcculsionTexture.center.set(100, 100);
roofAmbientOcculsionTexture.wrapS = THREE.RepeatWrapping;
roofAmbientOcculsionTexture.wrapT = THREE.RepeatWrapping;

roofRoughnessTexture.rotation = -0.12;
roofRoughnessTexture.center.set(100, 100);
roofRoughnessTexture.wrapS = THREE.RepeatWrapping;
roofRoughnessTexture.wrapT = THREE.RepeatWrapping;

const grassColorTexture = textureLoader.load("/textures/grass/color.jpg");
const grassAmbientOcculsionTexture = textureLoader.load(
  "/textures/grass/ambientOcclusion.jpg"
);
const grassNormalTexture = textureLoader.load("/textures/grass/normal.jpg");
const grassRoughnessTexture = textureLoader.load(
  "/textures/grass/roughness.jpg"
);

const bushTexture = textureLoader.load("/textures/grass/bush.jpg");

grassColorTexture.repeat.set(6, 6);
grassNormalTexture.repeat.set(6, 6);
grassAmbientOcculsionTexture.repeat.set(6, 6);
grassRoughnessTexture.repeat.set(6, 6);

grassColorTexture.wrapS = THREE.RepeatWrapping;
grassNormalTexture.wrapS = THREE.RepeatWrapping;
grassAmbientOcculsionTexture.wrapS = THREE.RepeatWrapping;
grassRoughnessTexture.wrapS = THREE.RepeatWrapping;

grassColorTexture.wrapT = THREE.RepeatWrapping;
grassNormalTexture.wrapT = THREE.RepeatWrapping;
grassAmbientOcculsionTexture.wrapT = THREE.RepeatWrapping;
grassRoughnessTexture.wrapT = THREE.RepeatWrapping;

let sprite = new THREE.TextureLoader().load("textures/rain/star.png");

/**
 * House
 */

const house = new THREE.Group();
scene.add(house);

const walls = new THREE.Mesh(
  new THREE.BoxBufferGeometry(4, 3, 4),
  new THREE.MeshStandardMaterial({
    map: bricksColorTexture,
    // aoMap: bricksAmbientOcculsionTexture,
    roughnessMap: bricksRoughnessTexture,
    normalMap: bricksNormalTexture,
  })
);
walls.geometry.setAttribute(
  "uv2",
  new THREE.Float32BufferAttribute(walls.geometry.attributes.uv.array, 4)
);
walls.position.y = 1.5;
house.add(walls);

const roof = new THREE.Mesh(
  new THREE.ConeBufferGeometry(3.5, 1.5, 4),
  new THREE.MeshStandardMaterial({
    map: roofColorTexture,
    // aoMap: roofAmbientOcculsionTexture,
    roughnessMap: roofRoughnessTexture,
    normalMap: roofNormalTexture,
    color: "#dd0000",
  })
);
roof.geometry.setAttribute(
  "uv2",
  new THREE.Float32BufferAttribute(roof.geometry.attributes.uv.array, 3)
);
roof.position.y = 3.75;
roof.rotation.y = Math.PI * 0.25;
house.add(roof);

const door = new THREE.Mesh(
  new THREE.PlaneBufferGeometry(2.5, 2.2),
  new THREE.MeshStandardMaterial({
    map: doorColorTexture,
    transparent: true,
    alphaMap: doorAlphaTexture,
    aoMap: doorAmbientOcclusionTexture,
    displacementMap: doorHeightTexture,
    displacementScale: 0.1,
    normalMap: doorNormalTexture,
    metalnessMap: doorMetalnessTexture,
    roughnessMap: doorRoughnessTexture,
  })
);

door.geometry.setAttribute(
  "uv2",
  new THREE.Float32BufferAttribute(door.geometry.attributes.uv.array, 2)
);
door.position.y = 0.9;
door.position.z = 2.01;
house.add(door);

const bushMaterial = new THREE.MeshStandardMaterial({
  map: grassColorTexture,
  aoMap: grassAmbientOcculsionTexture,
  roughness: 0.4,
  metalness: 0.1,
  normalMap: grassNormalTexture,
});
const bushGeometry = new THREE.SphereBufferGeometry(1, 16, 16);

const bush1 = new THREE.Mesh(bushGeometry, bushMaterial);
bush1.scale.set(0.5, 0.5, 0.5);
bush1.position.set(2, 0.35, 5);

const bush2 = new THREE.Mesh(bushGeometry, bushMaterial);
bush2.scale.set(0.4, 0.4, 0.4);
bush2.position.set(-1, 0.3, 2.2);

const bush3 = new THREE.Mesh(bushGeometry, bushMaterial);
bush3.scale.set(0.6, 0.75, 0.6);
bush3.position.set(1, 0.5, 2.3);
const bush4 = new THREE.Mesh(bushGeometry, bushMaterial);
bush4.scale.set(0.4, 0.35, 0.4);
bush4.position.set(2.65, 0.25, 5);
const bush5 = new THREE.Mesh(bushGeometry, bushMaterial);
bush5.scale.set(0.2, 0.2, 0.2);
bush5.position.set(2.4, 0.15, 5.4);

house.add(bush1, bush2, bush3, bush4, bush5);

const graves = new THREE.Group();
graves.rotation.y = Math.PI * 1.25;
scene.add(graves);

const graveGeometry = new THREE.BoxBufferGeometry(0.6, 0.8, 0.2);
const graveMaterial = new THREE.MeshStandardMaterial({
  color: "#808080",
  map: graveyardTexture,
  roughness: 0.5,
  metalness: 0.2,
});

for (let i = 0; i < 40; i++) {
  const angle = Math.random() * Math.PI * 1.5;
  const radius = 4 + Math.random() * 6;
  const x = Math.cos(angle) * radius;
  const z = Math.sin(angle) * radius;
  const graveRotation = (Math.random() - 0.5) * 0.2;
  const grave = new THREE.Mesh(graveGeometry, graveMaterial);
  grave.position.set(x, 0.3, z);
  grave.rotation.set(graveRotation, 0, graveRotation);
  graves.add(grave);
  grave.castShadow = true;
}

//rain

let rain;

let rainGeometry;
rainGeometry = new THREE.Geometry();
for (let i = 0; i < 6000; i++) {
  rain = new THREE.Vector3(
    (Math.random() - 0.5) * 40,
    (Math.random() - 0.5) * 40,
    (Math.random() - 0.5) * 40
  );
  rain.velocity = 0;
  rain.acceleration = 0.0005;
  rainGeometry.vertices.push(rain);
}

let rainMaterial = new THREE.PointsMaterial({
  color: 0xaaaaaa,
  size: 0.05,
  map: sprite,
  aoMap: sprite,
  alphaMap: sprite,
  sizeAttenuation: true,
});
let rainParticles;

rainParticles = new THREE.Points(rainGeometry, rainMaterial);
scene.add(rainParticles);

// Floor
const floor = new THREE.Mesh(
  new THREE.PlaneBufferGeometry(20, 20),
  new THREE.MeshStandardMaterial({
    map: grassColorTexture,
    aoMap: grassAmbientOcculsionTexture,
    roughness: 0.35,
    normalMap: grassNormalTexture,
  })
);
floor.geometry.setAttribute(
  "uv2",
  new THREE.Float32BufferAttribute(floor.geometry.attributes.uv.array, 2)
);
floor.rotation.x = -Math.PI * 0.5;
floor.position.y = 0;
scene.add(floor);

/**
 * Lights
 */
// Ambient light
const ambientLight = new THREE.AmbientLight("#b9d5ff", 0.2);
gui
  .add(ambientLight, "intensity")
  .min(0)
  .max(1)
  .step(0.001)
  .name("ambient intensity");
scene.add(ambientLight);

// Directional light
const moonLight = new THREE.DirectionalLight("#b9d5ff", 0.3);
moonLight.position.set(4, 5, -2);
gui.add(moonLight, "intensity").min(0).max(1).step(0.001).name("moon light");
gui.add(moonLight.position, "x").min(-5).max(5).step(0.001);
gui.add(moonLight.position, "y").min(-5).max(5).step(0.001);
gui.add(moonLight.position, "z").min(-5).max(5).step(0.001);
scene.add(moonLight);

// Door Light
const doorLight = new THREE.PointLight("#ff7d46", 1.1, 7);
doorLight.position.set(0, 2.2, 2.7);

house.add(doorLight);

// Ghosts

const ghost1 = new THREE.PointLight("#ADD8E6", 2, 3);
const ghost2 = new THREE.PointLight("#ADD8E6", 2, 3);
const ghost3 = new THREE.PointLight("#ADD8E6", 2, 3);

scene.add(ghost1, ghost2, ghost3);

/**
 * Sizes
 */
const sizes = {
  width: window.innerWidth,
  height: window.innerHeight,
};

window.addEventListener("resize", () => {
  // Update sizes
  sizes.width = window.innerWidth;
  sizes.height = window.innerHeight;

  // Update camera
  camera.aspect = sizes.width / sizes.height;
  camera.updateProjectionMatrix();

  // Update renderer
  renderer.setSize(sizes.width, sizes.height);
  renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2));
});

/**
 * Camera
 */
// Base camera
const camera = new THREE.PerspectiveCamera(
  75,
  sizes.width / sizes.height,
  0.1,
  100
);
camera.position.x = 4;
camera.position.y = 3;
camera.position.z = 7;
scene.add(camera);

// Controls
const controls = new OrbitControls(camera, canvas);
controls.enableDamping = true;

/**
 * Renderer
 */
const renderer = new THREE.WebGLRenderer({
  canvas: canvas,
});
renderer.setSize(sizes.width, sizes.height);
renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2));
renderer.setClearColor("#262837");
renderer.shadowMap.enabled = true;
renderer.shadowMap.type = THREE.PCFSoftShadowMap;
/**
 * Animate
 */

/**
 * shadows
 */
floor.receiveShadow = true;
moonLight.castShadow = true;
doorLight.castShadow = true;
ghost1.castShadow = true;
ghost2.castShadow = true;
ghost3.castShadow = true;
bush1.castShadow = true;
bush2.castShadow = true;
bush3.castShadow = true;
bush4.castShadow = true;
bush5.castShadow = true;
walls.castShadow = true;
const clock = new THREE.Clock();

doorLight.shadow.mapSize.width = 256;
doorLight.shadow.mapSize.height = 256;
doorLight.shadow.camera.far = 7;

moonLight.shadow.mapSize.width = 256;
moonLight.shadow.mapSize.height = 256;
moonLight.shadow.camera.far = 7;

ghost1.shadow.mapSize.width = 256;
ghost1.shadow.mapSize.height = 256;
ghost1.shadow.camera.far = 7;

ghost2.shadow.mapSize.width = 256;
ghost2.shadow.mapSize.height = 256;
ghost2.shadow.camera.far = 7;

ghost3.shadow.mapSize.width = 256;
ghost3.shadow.mapSize.height = 256;
ghost3.shadow.camera.far = 7;

const tick = () => {
  const elapsedTime = clock.getElapsedTime();
  // Update controls
  controls.update();

  // Render
  renderer.render(scene, camera);

  // Call tick again on the next frame
  window.requestAnimationFrame(tick);

  const ghost1Angle = elapsedTime * 0.5;
  ghost1.position.x = Math.cos(ghost1Angle) * 4;
  ghost1.position.z = Math.sin(ghost1Angle) * 4;
  ghost1.position.y = Math.sin(elapsedTime * 1);

  const ghost2Angle = -elapsedTime * 0.32;
  ghost2.position.x = Math.cos(ghost2Angle) * 4;
  ghost2.position.z = Math.sin(ghost2Angle) * 4;
  ghost2.position.y = Math.sin(elapsedTime * 1) + Math.sin(elapsedTime * 1.5);

  const ghost3Angle = -elapsedTime * 0.18;
  ghost3.position.x =
    Math.cos(ghost3Angle) * (7 + Math.sin(elapsedTime * 0.32));
  ghost3.position.z =
    Math.sin(ghost3Angle) * (7 + Math.sin(elapsedTime * 0.32));
  ghost3.position.y = Math.sin(elapsedTime * 5) + Math.sin(elapsedTime * 2);

  rainGeometry.vertices.forEach((p) => {
    p.velocity += p.acceleration;
    p.y -= p.velocity;
    if (p.y < -20) {
      p.y = 20;
      p.velocity = 0;
    }
  });
  rainGeometry.verticesNeedUpdate = true;
};

tick();
